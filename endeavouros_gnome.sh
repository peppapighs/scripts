#!/usr/bin/env bash

echo
echo "Installing Packages"

PKGS=(
    'adw-gtk3'
    'brave-bin'
    'discord'
    'fonts-tlwg'
    'gnome-backgrounds'
    'gnome-clocks'
    'gnome-shell-extension-alphabetical-grid-extension'
    'gnome-shell-extension-dash-to-panel'
    'gnome-system-monitor'
    'nerd-fonts-source-code-pro'
    'noto-fonts-cjk'
    'noto-fonts-emoji'
    'nvm'
    'papirus-icon-theme'
    'pinta'
    'postgresql'
    'rbenv'
    'ruby-build'
    'telegram-desktop'
    'ttf-windows'
    'vim'
    'visual-studio-code-bin'
    'yarn'
    'zoom'
    'zramd'
    'zsh'
)

for PKG in "${PKGS[@]}"; do
    yay -S --needed --noconfirm "$PKG"
done

sudo systemctl enable zramd

echo
echo "Done!"
