
#!/usr/bin/env bash

echo
echo "Removing Grub and Shim installation..."
if [[ $(efibootmgr | grep "arch") ]]; then
    sudo efibootmgr -b $(efibootmgr | grep "arch" | sed "s/Boot\(.*\)\*.*/\1/" -) -B
fi
if [[ $(efibootmgr | grep "Shim") ]]; then
    sudo efibootmgr -b $(efibootmgr | grep "Shim" | sed "s/Boot\(.*\)\*.*/\1/" -) -B
fi
sudo rm -rf /boot/efi/EFI/arch*

echo
echo "Installing Grub..."
sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi/ --modules="tpm" --sbat /usr/share/grub/sbat.csv
sudo sed -i "s/GRUB_DEFAULT=.*/GRUB_DEFAULT=saved/" /etc/default/grub
sudo sed -i "s/GRUB_GFXMODE=.*/GRUB_GFXMODE=1920x1080/" /etc/default/grub
sudo sed -i "/GRUB_SAVEDEFAULT=.*/s/#//" /etc/default/grub
sudo sed -i "s/GRUB_SAVEDEFAULT=.*/GRUB_SAVEDEFAULT=true/" /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg

echo
echo "Installing dependencies..."
yay -S --noconfirm --needed sbsigntools mokutil shim-signed

echo
if [ -f /etc/MOK.key ]; then
    echo "Machine owner key already generated..."
else
    echo "Creating machine owner key..."
    sudo openssl req -newkey rsa:4096 -nodes -keyout /etc/MOK.key -new -x509 -sha256 -days 36500 -subj "/CN=Machine Owner Key/" -out /etc/MOK.crt
    sudo openssl x509 -outform DER -in /etc/MOK.crt -out /etc/MOK.cer
fi

echo "Copying machine owner key certificate..."
sudo cp /etc/MOK.cer /boot/efi/

if [[ $(sbverify --list /boot/vmlinuz-linux 2> /dev/null | grep "signature certificates") ]]; then
    echo "Kernel signed..."
else
    echo "Signing kernel..."
    sudo sbsign --key /etc/MOK.key --cert /etc/MOK.crt --output /boot/vmlinuz-linux /boot/vmlinuz-linux
fi
if [[ $(sbverify --list /boot/efi/EFI/arch/grubx64.efi 2> /dev/null | grep "signature certificates") ]]; then
    echo "Grub signed..."
else
    echo "Signing Grub..."
    sudo sbsign --key /etc/MOK.key --cert /etc/MOK.crt --output /boot/efi/EFI/arch/grubx64.efi /boot/efi/EFI/arch/grubx64.efi
fi

echo
echo "Installing Shim..."
sudo cp /usr/share/shim-signed/shimx64.efi /boot/efi/EFI/arch/
sudo cp /usr/share/shim-signed/mmx64.efi /boot/efi/EFI/arch/
sudo efibootmgr --verbose --disk /dev/nvme0n1 --part 1 --create --label "Shim" --loader /EFI/arch/shimx64.efi

echo "Resetting MOK list..."
sudo mokutil --reset

echo
if [[ -d /etc/pacman.d/hooks ]]; then
    echo "Pacman hook directory exists..."
else
    echo "Creating Pacman hook directory..."
    sudo mkdir /etc/pacmand.d/hooks
fi

echo "Setting up shim hook..."
echo "[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = shim-signed
[Action]
Description = Copying Shim and MOK Manager to EFI partition
When = PostTransaction
Exec = /bin/sh -c 'cp /usr/share/shim-signed/shimx64.efi /boot/efi/EFI/arch/ && cp /usr/share/shim-signed/mmx64.efi /boot/efi/EFI/arch/'" | sudo tee /etc/pacman.d/hooks/998-copying_shim_and_mm_for_secureboot.hook > /dev/null

echo "Setting up pacman hook..."
echo "[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = linux
Target = linux-lts
Target = linux-hardened
Target = linux-zen
[Action]
Description = Signing kernel with Machine Owner Key for Secure Boot
When = PostTransaction
Exec = /usr/bin/find /boot/ -maxdepth 1 -name 'vmlinuz-*' -exec /usr/bin/sh -c 'if ! /usr/bin/sbverify --list {} 2>/dev/null | /usr/bin/grep -q \"signature certificates\"; then /usr/bin/sbsign --key /etc/MOK.key --cert /etc/MOK.crt --output {} {}; fi' ;
Depends = sbsigntools
Depends = findutils
Depends = grep" | sudo tee /etc/pacman.d/hooks/999-sign_kernel_for_secureboot.hook > /dev/null

echo
echo "Done"
