#!/usr/bin/env bash

echo
echo "Installing Plasma"

PLASMA=(
    'bluedevil'
    'breeze-gtk'
    'egl-wayland'
    'kde-gtk-config'
    'khotkeys'
    'kinfocenter'
    'kscreen'
    'ksshaskpass'
    'kwallet-pam'
    'kwayland-integration'
    'plasma-desktop'
    'plasma-disks'
    'plasma-firewall'
    'plasma-nm'
    'plasma-pa'
    'plasma-systemmonitor'
    'plasma-thunderbolt'
    'plasma-wayland-session'
    'powerdevil'
    'sddm-kcm'
    'xdg-desktop-portal-kde'

    'ark'
    'dolphin'
    'kcalc'
    'konsole'
    'kolourpaint'
    'kwrite'
    'okular'
    'spectacle'
)

for PKG in "${PLASMA[@]}"; do
    yay -S --needed --noconfirm "$PKG"
done

sudo systemctl enable sddm

echo
echo "Installing Packages"

PKGS=(
    'apple-fonts'
    'brave-bin'
    'dbeaver'
    'discord'
    'fonts-tlwg'
    'mysql'
    'nerd-fonts-sf-mono'
    'noto-fonts'
    'noto-fonts-cjk'
    'noto-fonts-emoji'
    'nvm'
    'onlyoffice'
    'openssh'
    'papirus-icon-theme'
    'postgresql'
    'python2'
    'rbenv'
    'ruby-build'
    'telegram-desktop'
    'vim'
    'visual-studio-code-bin'
    'yarn'
    'zoom'
    'zramd'
    'zsh'
)

for PKG in "${PKGS[@]}"; do
    yay -S --needed --noconfirm "$PKG"
done

sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo -iu postgres initdb -D /var/lib/postgres/data

sudo systemctl enable bluetooth
sudo systemctl enable mysql
sudo systemctl enable paccache.timer
sudo systemctl enable postgresql
sudo systemctl enable sshd
sudo systemctl enable zramd

echo
echo "Configuring vimrc..."
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

echo
echo "Configuring zsh..."
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
sed -i "s/ZSH_THEME=.*/ZSH_THEME=\"powerlevel10k\/powerlevel10k\"/" ~/.zshrc
sed -i "s/plugins=(\(.*\))/plugins=(\1 sudo zsh-autosuggestions zsh-syntax-highlighting)/" ~/.zshrc

echo >> ~/.zshrc
echo "source /usr/share/nvm/init-nvm.sh" >> ~/.zshrc
echo >> ~/.zshrc
echo "eval \"\$(rbenv init - zsh)\"" >> ~/.zshrc

echo
echo "Configuring environment..."
sudo sed -i "s/BROWSER=.*/BROWSER=brave/" /etc/environment
sudo sed -i "s/EDITOR=.*/EDITOR=vim/" /etc/environment

yay -Qtdq | yay -Rns --noconfirm -

echo
echo "Done!"

