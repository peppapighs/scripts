#!/usr/bin/env bash

echo
echo "Configuring makepkg..."
sudo sed -i "/MAKEFLAGS=.*/s/#//" /etc/makepkg.conf
sudo sed -i "s/MAKEFLAGS=.*/MAKEFLAGS=\"-j\$(nproc)\"/" /etc/makepkg.conf
sudo sed -i "s/COMPRESSXZ=(\(.*\) -)/COMPRESSXZ=(\1 -T 0 -)/" /etc/makepkg.conf

echo
echo "Installing yay..."
git clone https://aur.archlinux.org/yay.git /tmp/yay
(cd /tmp/yay && makepkg -si)

echo
echo "Installing microcode..."
yay -S --noconfirm --needed intel-ucode amd-ucode
sudo grub-mkconfig -o /boot/grub/grub.cfg

echo
echo "Configuring pacman..."
sudo sed -i "/Color/s/#//" /etc/pacman.conf
sudo sed -i "/\[multilib\]/s/#//" /etc/pacman.conf
sudo sed -i "/\[multilib\]/{n; s/#//}" /etc/pacman.conf
sudo pacman -Syyu

echo
echo "Installing Plasma"

PLASMA=(
    'bluedevil'
    'breeze-gtk'
    'egl-wayland'
    'kde-gtk-config'
    'khotkeys'
    'kinfocenter'
    'kscreen'
    'ksshaskpass'
    'kwallet-pam'
    'kwayland-integration'
    'plasma-desktop'
    'plasma-disks'
    'plasma-firewall'
    'plasma-nm'
    'plasma-pa'
    'plasma-systemmonitor'
    'plasma-thunderbolt'
    'plasma-wayland-session'
    'powerdevil'
    'sddm-kcm'
    'xdg-desktop-portal-kde'

    'ark'
    'dolphin'
    'kcalc'
    'konsole'
    'kolourpaint'
    'kwrite'
    'okular'
    'spectacle'

    'pipewire'
    'pipewire-alsa'
    'pipewire-jack'
    'pipewire-pulse'
    'gst-plugin-pipewire'
    'libpulse'
    'wireplumber'

    'ntfs-3g'
    'power-profiles-daemon'
    'upower'
    'wget'
)

for PKG in "${PLASMA[@]}"; do
    yay -S --needed --noconfirm "$PKG"
done

sudo systemctl enable sddm

echo
echo "Installing Packages"

PKGS=(
    'apple-fonts'
    'brave-bin'
    'discord-canary'
    'downgrade'
    'fonts-tlwg'
    'mysql'
    'neofetch'
    'nerd-fonts-sf-mono'
    'noto-fonts'
    'noto-fonts-cjk'
    'noto-fonts-emoji'
    'ntp'
    'nvm'
    'openssh'
    'postgresql'
    'python2'
    'rbenv'
    'rsync'
    'ruby-build'
    'telegram-desktop'
    'ufw'
    'vim'
    'visual-studio-code-bin'
    'yarn'
    'zoom'
    'zramd'
    'zsh'
)

for PKG in "${PKGS[@]}"; do
    yay -S --needed --noconfirm "$PKG"
done

sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo -iu postgres initdb -D /var/lib/postgres/data

sudo systemctl enable bluetooth
sudo systemctl enable mysql
sudo systemctl enable ntpd
sudo systemctl enable paccache.timer
sudo systemctl enable postgresql
sudo systemctl enable sshd
sudo systemctl enable zramd

echo
echo "Configuring vimrc..."
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh

echo
echo "Configuring zsh..."
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
sed -i "s/ZSH_THEME=.*/ZSH_THEME=\"powerlevel10k\/powerlevel10k\"/" ~/.zshrc
sed -i "s/plugins=(\(.*\))/plugins=(\1 sudo zsh-autosuggestions zsh-syntax-highlighting)/" ~/.zshrc

echo >> ~/.zshrc
echo "source /usr/share/nvm/init-nvm.sh" >> ~/.zshrc
echo >> ~/.zshrc
echo "eval \"\$(rbenv init - zsh)\"" >> ~/.zshrc

echo
echo "Configuring environment..."
sudo sed -i "s/BROWSER=.*/BROWSER=brave/" /etc/environment
sudo sed -i "s/EDITOR=.*/EDITOR=vim/" /etc/environment

yay -Qtdq | yay -Rns --noconfirm -

echo
echo "Done!"
