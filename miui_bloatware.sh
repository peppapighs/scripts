#!/usr/bin/env bash

echo
echo "Uninstalling MIUI Bloatware"

PKGS = (
    'com.google.android.apps.docs'
    'com.google.android.apps.maps'
    'com.google.android.apps.photos'
    'com.google.android.apps.tachyon'
    'com.google.android.feedback'
    'com.google.android.gm'
    'com.google.android.googlequicksearchbox'
    'com.google.android.marvin.talkback'
    'com.google.android.music'
    'com.google.android.syncadapters.calendar'
    'com.google.android.syncadapters.contacts'
    'com.google.android.talk'
    'com.google.android.tts'
    'com.google.android.videos'
    'com.google.android.youtube'
    
    'com.netflix.partner.activation'
    'com.zhiliaoapp.musically'
    'ru.yandex.searchplugin'    'com.tencent.soter.soterserver'
    'cn.wps.xiaomi.abroad.lite'
    'com.miui.videoplayer'
    'com.miui.player'
    'com.mi.globalbrowser'
    'com.xiaomi.midrop'
    'com.miui.yellowpage'
    'com.miui.gallery'
    'com.miui.android.fashiongallery'
    'com.miui.bugreport'
    'com.miui.miservice'
    'com.miui.weather2'
    'com.miui.hybrid'
    'com.miui.hybrid.accessory'
    'com.miui.global.packageinstaller'
    'com.android.stk'
    'com.android.stk2'
    'com.miui.extraphoto'
    'com.android.chrome'
    'com.miui.compass'
    'com.mi.android.globalFileexplorer'
    'com.miui.mediaeditor'
    'com.google.android.apps.subscriptions.red'
    'com.google.android.apps.magazines'
    'com.google.android.apps.podcasts'
    'com.iqiyi.i18n'
    'com.lazada.android'
    'com.duokan.phone.remotecontroller'
    'com.miui.mishare.connnectivity'
    'com.netflix.mediaclient'
    'com.miui.notes'
    'com.android.soundrecorder'
    'com.xiaomi.midrop'
    'com.spotify.music'
    'com.miui.weather2'
    'cn.wps.moffice_eng'
    'com.google.android.apps.youtube.music'
    'com.tencent.igxiaomi'

    'com.swiftkey.languageprovider'
    'com.swiftkey.swiftkeyconfigurator'
    'com.xiaomi.account'
    'com.xiaomi.discover'
    'com.xiaomi.glgm'
    'com.xiaomi.joyose'
    'com.xiaomi.location.fused'
    'com.xiaomi.micloud.sdk'
    'com.xiaomi.midrop'
    'com.xiaomi.mipicks'
    'com.xiaomi.miplay_client'
    'com.xiaomi.mirecycle'
    'com.xiaomi.oversea.ecom'
    'com.xiaomi.payment'
    'com.xiaomi.providers.appindex'
    'com.xiaomi.xmsf'
)

for PKG in "${PKGS[@]}"; do
    adb shell pm uninstall "$PKG"
    adb shell pm uninstall --user 0 "$PKG"
done

echo
echo "Done!"
